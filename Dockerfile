FROM python:3.7.3


ARG BUILDDIR
ARG LISTEN_INTERFACE
ARG LISTEN_PORT
ENV FLASK_ENV development
ENV FLASK_APP chat:app
ENV DB_CONN_STRING "postgresql://chat:******@db:5432/chat"

COPY . ${BUILDDIR}

RUN python -m pip install -r ${BUILDDIR}/requirements.txt

WORKDIR ${BUILDDIR}

RUN echo "#!/bin/bash" > entrypoint.sh && \
 echo "flask run --host ${LISTEN_INTERFACE} --port ${LISTEN_PORT}" >> entrypoint.sh && \
 chmod +x ./entrypoint.sh
 
 
CMD [ "./entrypoint.sh" ]