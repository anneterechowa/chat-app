import werkzeug.exceptions as exc
from flask import request, g
from flask.views import MethodView
from chat.utils import jsonify, login_required
from sqlalchemy.orm import contains_eager

from chat.controllers.v1.serializers import MessageSchema
from chat.models import Chat, db, Message

schema = MessageSchema()


class MessagesResource(MethodView):
    @login_required
    def get(self, unit_id):
        args = self._get_args(unit_id)
        chat = db.session.query(Chat) \
            .join(Chat.messages) \
            .options(contains_eager(Chat.messages)) \
            .filter(Chat.unit_id == unit_id) \
            .filter(Chat.student_id == args['student_id']) \
            .order_by(Message.created_at.asc()) \
            .one_or_none()

        if not chat:
            return jsonify([])

        users = self._fetch_users(chat.messages)
        with schema.users_context(users):
            result = schema.dump(chat.messages, many=True)

        return jsonify(result.data)

    @login_required
    def post(self, unit_id):
        """Create message object."""
        args = self._get_args(unit_id)
        return self._create_message(
            unit_id=unit_id,
            student_id=args['student_id'],
            author_id=args['author_id']
        )

    def _fetch_users(self, messages):
        users_dict = {}
        user_ids = {msg.author_id for msg in messages}
        users_list = []

        for user in users_list:
            users_dict[user['id']] = user

        return users_dict

    def _get_args(self, unit_id):
        """Parse args that are necessary for creating message object."""
        current_user_id = g.user['id']
        user_id = request.args.get('user_id', type=int)

        create_args = {
            'unit_id': unit_id
        }

        if user_id is not None and user_id != current_user_id:
            # Got teacher. Teacher sends user_id
            # as a query string parameter
            self._ensure_teacher(current_user_id, unit_id)
            create_args['author_id'] = current_user_id
            create_args['student_id'] = user_id

        else:
            # Got student. Student is identified just by
            # `g.user['id']`
            create_args['student_id'] = current_user_id
            create_args['author_id'] = current_user_id

        return create_args

    def _ensure_teacher(self, user_id, unit_id):
        """Check if requesting user is a teacher on the course."""
        return True

    def _get_chat(self, unit_id, student_id):
        """Get existing chat or create a new one."""
        chat = db.session.query(Chat) \
            .filter_by(unit_id=unit_id, student_id=student_id) \
            .one_or_none()

        if not chat:
            # Check if user is really registered on course
            self._ensure_assigned_task(student_id, unit_id)
            chat = Chat(unit_id=unit_id, student_id=student_id)

        return chat

    def _create_message(self, unit_id, student_id, author_id):
        """Create message in chat."""
        message = schema.load_or_reject(request.get_json(force=True))

        chat = self._get_chat(unit_id, student_id)

        db.session.add(chat)
        db.session.flush()

        message.chat = chat
        message.author_id = author_id

        db.session.add(message)
        db.session.commit()

        return jsonify(schema.dump(message).data, 201)

    def _ensure_assigned_task(self, user_id, unit_id):
        """Ensure that user has been registered."""
        pass

    # delete function 
    @login_required
    def delete(self,message_id: int):
        message = db.session.query(Message).filter(Message.id==message_id)\
            .one_or_none()
        if message is None:
            raise exc.NotFound('Message with this id was not found')
        if message.author_id != g.user['id']:
            raise exc.Forbidden('fgfg')
        db.session.delete(message)
        db.session.commit()

        return jsonify({})
