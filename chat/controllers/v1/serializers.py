from contextlib import contextmanager
from marshmallow import Schema, fields, post_load
from werkzeug.exceptions import BadRequest

from chat.models import Message


class BaseSchema(Schema):
    def load_or_reject(self, *args, **kwargs):
        """Load ORM object or raise 400.

        Don't let error-handling copy-pase code
        flood your views!

        It's an applicant to be moved to flask_utils.
        """
        result = super(Schema, self).load(*args, **kwargs)

        if result.errors:
            raise BadRequest(result.errors)

        return result.data


class MessageSchema(BaseSchema):
    id = fields.Integer(dump_only=True)
    text = fields.String(required=True)
    created_at = fields.DateTime()
    author = fields.Method(dump_only=True, serialize='_dump_author')

    @post_load
    def _load_message(self, data):
        return Message(**data)

    def _dump_author(self, message):
        users = self.context.get('users', {})
        author = users.get(message.author_id)

        if not author:
            # Unexpected behaviour
            return None

        return '{} {} {}'.format(
            author['last_name'], author['first_name'], author['surname'])

    @contextmanager
    def users_context(self, users):
        self.context['users'] = users
        yield
        self.context.pop('users')


class ChatSchema(BaseSchema):
    id = fields.Integer(dump_only=True)
    assigned_task_id = fields.Integer(required=True)
    unit_id = fields.Integer(required=True)
    messages = fields.Nested(MessageSchema, many=True)
    participants = fields.List(fields.Integer(), dump_only=True)
