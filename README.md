Backend для сервиса комментариев на flask + sqlalchemy

Для тестирования используется docker-compose (чтобы поднимать рядом postgres, а не sqlite)

Собирается wheel, rpm спека лежит в rpm/chat.spec

RPM собирается внутри контейнера с centos - из него надо вытащить собранную rpm-ку /root/rpmbuild/RPMS/x86_64/chat-0.0.1-1.x86_64.rpm

*.whl публикуется в качестве артефактов, а вот rpm-ку вытащить из docker container быстро не удалось(